﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private string tableName;
        private bool errorStatus = false;
        Exception Err;

        enum RootTree {Sprzedaż=1, Zakup, Magazyn, Finanse};

        public struct connectionInfo
        {
            public string userName;
            public string userPassword;
            public string dataBaseName;
            public string catalogName;
            public connectionInfo(string Name, string Password, string nameBase,string nameCatalog)
            {
                userName = Name;
                userPassword = Password;
                dataBaseName = nameBase;
                catalogName = nameCatalog;
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent == null)
            {
                toolStripStatusLabel2.Text = "Akcja: Wybierz skutek";
                return;
            }

            switch (e.Node.Parent.Text)
            {
                case "Sprzedaż":
                    break;
                case "Zakup":
                    break;
                case "Magazyn":
                    break;
                case "Finanse":
                    break;
                case null:
                    break;
            }

            toolStripStatusLabel2.Text = "";
        }

        private SqlConnection connectToDataBase(connectionInfo Data)
        {
            SqlConnection polaczenie=null;
            string informacje = "Data Source=" + Data.dataBaseName + ";Initial Catalog=" + Data.catalogName + ";User ID=" + Data.userName + ";Password=" + Data.userPassword;
            try
            {
                polaczenie = new SqlConnection(informacje);
                polaczenie.Open();
                toolStripStatusLabel1.Text = "Status: Połączono";
                errorStatus = false;
                Err = null;
            }
            catch (Exception error)
            {
                toolStripStatusLabel1.Text = "Status: Nie udało się nazwiązać połączenia :( Kliknij na poniższy pasek aby dowiedzieć się szczegółów";
                errorStatus = true;
                Err = error;
            }
            return polaczenie;
        }
        private void disconnectDataBase(SqlConnection polaczenie)
        {
            try
            {
                polaczenie.Close();
            }
            catch(Exception error)
            {
                toolStripStatusLabel1.Text = "Status: Nie udało się rozwiazac połączenia :( Kliknij na poniższy pasek aby dowiedzieć się szczegółów";
                errorStatus = true;
                Err = error;
            }
        }
        private connectionInfo initData()
        {
            connectionInfo Data = new connectionInfo(@"Marek", "12345", @"OEM-KOMPUTER\SQLEXPRESS", @"Magazine");
            return Data;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection polaczenie;
            connectionInfo danePolaczenia;
            danePolaczenia = initData();
            polaczenie = connectToDataBase(danePolaczenia);
            //Code hear:

            disconnectDataBase(polaczenie);
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if(errorStatus == true)
            {
                MessageBox.Show(Err.ToString());
            }
        }

        private void zakończToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }
    }
}
