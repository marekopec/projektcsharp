﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Faktury sprzedaży");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Korekty sprzedaży");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Sprzedaż detaliczna");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Zwroty detaliczny");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Zamówienia od klientów");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Sprzedaż", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Faktury zakupu");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Korekty zakupu");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Zamówienia do dostawców");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Zakup", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Wydania magazynowe");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Wydania odbiorcy");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Przyjecia magazynowe");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Przyjecia dostawcy");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Inweratryzacja");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Korekty kosztów");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Magazyn", new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Dokumenty kasowe");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Raporty kasowe");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Finanse", new System.Windows.Forms.TreeNode[] {
            treeNode18,
            treeNode19});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakończToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 27);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Węzeł13";
            treeNode1.Text = "Faktury sprzedaży";
            treeNode2.Name = "Węzeł14";
            treeNode2.Text = "Korekty sprzedaży";
            treeNode3.Name = "Węzeł15";
            treeNode3.Text = "Sprzedaż detaliczna";
            treeNode4.Name = "Węzeł16";
            treeNode4.Text = "Zwroty detaliczny";
            treeNode5.Name = "Węzeł17";
            treeNode5.Text = "Zamówienia od klientów";
            treeNode6.Name = "sale";
            treeNode6.Tag = "Glowny1";
            treeNode6.Text = "Sprzedaż";
            treeNode7.Name = "Węzeł18";
            treeNode7.Text = "Faktury zakupu";
            treeNode8.Name = "Węzeł19";
            treeNode8.Text = "Korekty zakupu";
            treeNode9.Name = "Węzeł20";
            treeNode9.Text = "Zamówienia do dostawców";
            treeNode10.Name = "purchase";
            treeNode10.Text = "Zakup";
            treeNode11.Name = "Węzeł21";
            treeNode11.Text = "Wydania magazynowe";
            treeNode12.Name = "Węzeł22";
            treeNode12.Text = "Wydania odbiorcy";
            treeNode13.Name = "Węzeł23";
            treeNode13.Text = "Przyjecia magazynowe";
            treeNode14.Name = "Węzeł24";
            treeNode14.Text = "Przyjecia dostawcy";
            treeNode15.Name = "Węzeł25";
            treeNode15.Text = "Inweratryzacja";
            treeNode16.Name = "Węzeł26";
            treeNode16.Text = "Korekty kosztów";
            treeNode17.Name = "warehouse";
            treeNode17.Text = "Magazyn";
            treeNode18.Name = "Węzeł27";
            treeNode18.Text = "Dokumenty kasowe";
            treeNode19.Name = "Węzeł28";
            treeNode19.Text = "Raporty kasowe";
            treeNode20.Name = "finance";
            treeNode20.Text = "Finanse";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode10,
            treeNode17,
            treeNode20});
            this.treeView1.Size = new System.Drawing.Size(189, 353);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(207, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(739, 426);
            this.dataGridView1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.opcjeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(958, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zakończToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // zakończToolStripMenuItem
            // 
            this.zakończToolStripMenuItem.Name = "zakończToolStripMenuItem";
            this.zakończToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.zakończToolStripMenuItem.Text = "Zakończ";
            this.zakończToolStripMenuItem.Click += new System.EventHandler(this.zakończToolStripMenuItem_Click);
            // 
            // opcjeToolStripMenuItem
            // 
            this.opcjeToolStripMenuItem.Name = "opcjeToolStripMenuItem";
            this.opcjeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.opcjeToolStripMenuItem.Text = "Opcje";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(51, 386);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Sprawdzenie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 629);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(958, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusLabel1.Text = "Status: -";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 651);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Program sprzedazowy";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakończToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcjeToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
    }
}

